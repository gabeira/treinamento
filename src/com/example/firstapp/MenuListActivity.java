package com.example.firstapp;

//import android.app.Activity;
import android.app.ListActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MenuListActivity extends ListActivity {

	private static String[] itens = { 
		"Life Cicle", 
		"Call: 555", 
		"WWW", 
		"Send Params",
		"Notify statusbar",
		"Broadcast Sample",
		"Parse JSON"};
	
	
	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		//setListAdapter( new ArrayAdapter<String>( this, android.R.layout.simple_list_item_1, itens ) );
		setListAdapter( new MyAdapter( ) );
	}

	@Override
	protected void onListItemClick( ListView l, View v, int position, long id ) {
		switch ( position ) {
		case 0:
			startActivity( new Intent( this, LifeCicleActivity.class ) );
			break;
		case 1:
			Uri uri = Uri.parse( "tel:555" );
			startActivity( new Intent( Intent.ACTION_CALL, uri ) );
			break;
		case 2:
			uri = Uri.parse( "http://www.terra.com.br" );
			startActivity( new Intent( Intent.ACTION_VIEW, uri ) );
			break;
		case 3:
			startActivity( new Intent( this, FirstWndActivity.class ) );
			break;
		case 4:
			sendNotification( "Aten��o!", "Alerta!", "Mensagem" );
			break;
		case 5:
			startActivity( new Intent( this, BroadcastSampleActivity.class ) );
			break;
		case 6:
		    Toast.makeText( this, itens[ position ], Toast.LENGTH_LONG ).show();
		    
		    Intent it = new Intent( this, ParseJSON.class );
		    it.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );   
		    startActivity( it );

		    break;
		default:
			Toast.makeText( this, itens[ position ], Toast.LENGTH_SHORT ).show();
			break;
		}
	}
	
	//
	private class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return itens.length;
		}

		@Override
		public Object getItem(int position) {
			return itens[ position ];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView( int position, View v, ViewGroup parent ) {
			LayoutInflater inflater = (LayoutInflater) getSystemService( LAYOUT_INFLATER_SERVICE );
			
			RelativeLayout layout = ( RelativeLayout )inflater.inflate( R.layout.item_list , null );
			TextView title = ( TextView )layout.findViewById( R.id.title );
			title.setText( itens[ position ] );
			
			return layout;
		}		
	}	

	private void sendNotification( String msgStatusbar, CharSequence msgTitle, CharSequence msg ) {
		
		boolean canVibrate = true;
		boolean canSound   = true;
		
		// Notificacao
		NotificationManager nm =( NotificationManager )getSystemService( Context.NOTIFICATION_SERVICE );
		
//		Notification n = new Notification.Builder( this )
//    	.setContentTitle( msgTitle )
//    	.setContentText( msgStatusbar )
//    	.setSmallIcon( R.drawable.icon_notificacao )
//    	//.setLargeIcon(aBitmap)
//    	.build();
		
		Notification n = new Notification( R.drawable.icon_notificacao, msgStatusbar, System.currentTimeMillis() );
		
		// Acao
		PendingIntent p = PendingIntent.getActivity( this, 0, new Intent( this, MenuListActivity.class ), 0 );
		n.setLatestEventInfo( this, msgTitle, msg, p );
		
		if ( canVibrate ){
			n.vibrate = new long[] { 100, 250, 100, 500 };
		}
		
		if ( canSound ) {
			//n.sound = Uri.parse( "android.resource://com.packagename.org/raw/alert" );
			n.sound = RingtoneManager.getDefaultUri( RingtoneManager.TYPE_NOTIFICATION );
		}
		
		nm.notify( R.string.app_name, n );
	}

}
