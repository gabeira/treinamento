
package com.example.firstapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class ParseJSON extends Activity {

    /** Called when the activity is first created. */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parse_json);
        Log.i("FIRSTAPP", ParseJSON.class.getName() + " parse...");
        
        try {
            String readTwitterFeed = readTwitterFeed();
            Log.i("FIRSTAPP", readTwitterFeed);
            //JSONArray jsonArray = new JSONArray(readTwitterFeed);
           // Log.i("FIRSTAPP", ParseJSON.class.getName() + " Number of entries " + jsonArray.length());
            /*for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Log.i("FIRSTAPP", ParseJSON.class.getName() + " - " + jsonObject.getString("text"));
            }*/
        } catch (Exception e) {
            Log.e("FIRSTAPP", " Failed to read Twitter");
            e.printStackTrace();
        }
    }

    public String readTwitterFeed() {
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        
        // HttpGet httpGet = new HttpGet("http://twitter.com/statuses/user_timeline/vogella.json");
        HttpGet httpGet = new HttpGet("http://search.twitter.com/search.json?q=android");
        Log.i("FIRSTAPP", " 2Httpget");
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } else {
                Log.e("FIRSTAPP", ParseJSON.class.getName() + " 1Failed to download file");
            }
        } catch (ClientProtocolException e) {
            Log.e("FIRSTAPP", ParseJSON.class.getName() + " 2Failed to download file");
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("FIRSTAPP", ParseJSON.class.getName() + " 3Failed to download file");
            e.printStackTrace();
        }
        return builder.toString();
    }

}
