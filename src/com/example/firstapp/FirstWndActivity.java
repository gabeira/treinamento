package com.example.firstapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class FirstWndActivity extends Activity {

	static final int VERIFICAR_NOME = 1000;
	EditText edText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.first_wnd );
		
		edText = ( EditText )findViewById( R.id.myname );
	}
	
	public void send( View v ) {
		Intent it = new Intent( this, SecondWndActivity.class );
		it.putExtra( "FIRSTWND.NAME" , edText.getText().toString() );
		
//		startActivity( it );
		startActivityForResult( it, VERIFICAR_NOME );
	}

	@Override
	protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
		switch ( requestCode ) {
		case VERIFICAR_NOME:
			String result = data.getStringExtra( "RESULT.SECOND.WND" );
			Toast.makeText( this, result, Toast.LENGTH_SHORT ).show();
			
			if( resultCode == RESULT_CANCELED ) {
				edText.setText( "" );
			}
			break;
		default:
			Log.i( "FIRSTAPP" , "Retorno desconhecido" );
			break;
		}
	}	
}
