package com.example.firstapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class BroadcastSampleActivity extends Activity {

	Button reload;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.broadcast_sample );
		
		reload = ( Button )findViewById( R.id.reload );	
	}
	
	public void reload( View v ) {
		Toast.makeText( this, "Executing reload...", Toast.LENGTH_LONG ).show();
	}	

	@Override
	protected void onStart() {
		super.onStart();
		registerReceiver( mConnectivityCheckReceiver, filter );
	}

	@Override
	protected void onStop() {
		super.onStop();
		unregisterReceiver( mConnectivityCheckReceiver );
	}

	//  Receiver
	static final String ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
	static final IntentFilter filter = new IntentFilter( ACTION );
	private final BroadcastReceiver mConnectivityCheckReceiver = new BroadcastReceiver() {
		/**
		 * @see android.content.BroadcastReceiver#onReceive(Context,Intent)
		 */
		@Override
		public void onReceive( Context context, Intent intent ) {
			String action = intent.getAction();

			Log.d( "FIRSTAPP", "----------> Action: " + action );
			
	        boolean noConnectivity = intent.getBooleanExtra( ConnectivityManager.EXTRA_NO_CONNECTIVITY, false );
	        String reason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
	        boolean isFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);
	        NetworkInfo currentNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
	        NetworkInfo otherNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);
	        
	        Log.i( "FIRSTAPP", "Status : " + noConnectivity + ", Reason :" + reason + ", FailOver :" + isFailover + ", Current Network Info : " + currentNetworkInfo + ", OtherNetwork Info :" + otherNetworkInfo);

	        boolean mStatus = noConnectivity;
	        
	        reload.setEnabled( mStatus );
	        
	        Log.d( "FIRSTAPP", "----------> Status :" + mStatus);
		}
	};

}
