package com.example.firstapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class SecondWndActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.second_wnd );
		
		Intent it = getIntent();
		if ( it.getStringExtra( "FIRSTWND.NAME" ) != null ) {
			TextView text = ( TextView )findViewById( R.id.name );
			text.setText( it.getStringExtra( "FIRSTWND.NAME" ) );
		}
	}
	
	public void done( View v ) {
		Intent data = getIntent();
		data.putExtra( "RESULT.SECOND.WND", "Nome correto!!" );
		setResult( RESULT_OK, data );
		finish();
	}
	
	public void nops( View v ) {
		Intent data = getIntent();
		data.putExtra( "RESULT.SECOND.WND", "Nome incorreto!!" );
		setResult( RESULT_CANCELED, data );
		finish();
	}
}