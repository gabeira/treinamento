package com.example.firstapp;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class LifeCicleActivity extends Activity {

	static final String STATE_SCORE = "playerScore";
	static final String STATE_LEVEL = "playerLevel";
	
	private int mCurrentScore;
	private int mCurrentLevel;
	
	TextView scoreAndLevel;
	
	private SharedPreferences.Editor editor;
	private SharedPreferences settings;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.linear );
        
        settings = this.getSharedPreferences( "com.example.firstapp", 0 );
		editor = settings.edit();
		
		mCurrentScore = settings.getInt( STATE_SCORE , 0 );
		mCurrentLevel = settings.getInt( STATE_LEVEL , 0 );
        
        scoreAndLevel = ( TextView )findViewById( R.id.scoreAndLevel );
        scoreAndLevel.setText( mCurrentScore + "/" + mCurrentLevel );
        
        Log.i( "FIRSTAPP", "onCreate" );
    }
    
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
    	Log.i( "FIRSTAPP", "onSaveInstanceState" );
    	
    	// Save the user's current game state
        savedInstanceState.putInt( STATE_SCORE, mCurrentScore );
        savedInstanceState.putInt( STATE_LEVEL, mCurrentLevel );
        
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState( savedInstanceState );
    }
    
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);
       
        // Restore state members from saved instance
        mCurrentScore = savedInstanceState.getInt( STATE_SCORE );
        mCurrentLevel = savedInstanceState.getInt( STATE_LEVEL );
        
        scoreAndLevel.setText( mCurrentScore + "/" + mCurrentLevel );
        
        Log.i( "FIRSTAPP", "onRestoreInstanceState" );
    }

    public void hit( View v ) {
    	mCurrentScore = ( ++mCurrentScore % 10 == 0 ) ? 0 : mCurrentScore;
    	mCurrentLevel = ( mCurrentScore == 0 ) ? ++mCurrentLevel : mCurrentLevel;
    	scoreAndLevel.setText( mCurrentScore + "/" + mCurrentLevel );
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		 getMenuInflater().inflate( R.menu.main_menu, menu );
	     return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch ( item.getItemId() )  {
		case R.id.save:
			Log.i( "FIRSTAPP", "save" );
			editor.putInt( STATE_SCORE, mCurrentScore );
			editor.putInt( STATE_LEVEL, mCurrentLevel );
			editor.commit();
			break;
		case R.id.finish:
			Log.i( "FIRSTAPP", "finish" );
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		
		finish();
		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i( "FIRSTAPP", "onDestroy" );
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.i( "FIRSTAPP", "onPause" );
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.i( "FIRSTAPP", "onRestart" );
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.i( "FIRSTAPP", "onResume" );
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.i( "FIRSTAPP", "onStart" );
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.i( "FIRSTAPP", "onStop" );
	}
}
